#! /usr/bin/python2

import os
# import argparse
import subprocess
import platform

# # ------- VARIABLES
debug = False
terraform = None
ansible = None
ssh = None
inst_type = ("t2.nano", "t2.micro", "t2.small", "t2.medium", 
    "t2.large", "t2.xlarge", "t2.2xlarge", "m4.large", "m4.xlarge", 
    "m4.2xlarge", "m4.4xlarge", "m4.10xlarge", "m4.16xlarge")
key = "ssh-keygen -t rsa -b 4096 -f ./ansible-key -N ''"

# try:
#     subprocess.check_call(['executable'])
# except subprocess.CalledProcessError:
#     pass # handle errors in the called executable
# except OSError:
#     pass # executable not found

# ------- MODULES
def ask_ok(prompt, retries=1):
    while True:
        ok = raw_input(prompt)
        if ok in ('y', 'ye', 'yes'):
            return True
        if ok in ('n', 'no', 'nop', 'nope', ''):
            return False
        retries = retries - 1
        if retries < 0:
            return True

def cmd_exists(cmd):
    try:
        subprocess.check_call([cmd])
    except subprocess.CalledProcessError:
        return True # Handle errors in the called executable
    except OSError:
        return False

def install_aws():
    print(str("aws is not installed. Installing..."))
    if not debug:
        os.system("sudo pip install awscli -q")
    else:
        os.system("sudo pip install awscli")
    os.system("aws configure")

def config_aws():
    if not os.path.isfile("~/.aws"):  # File exists ?
        print("Not logged in to AWS, if you dont have the credentials get it from AWS Console")
        raw_input("Press Enter to continue...")
        os.system("aws configure")

def create_key():
    subprocess.call("ssh-keygen -t rsa -b 4096 -f ./ansible-key -N ''", shell=True, 
        stdout=subprocess.PIPE, stderr=subprocess.PIPE) == 0

def clear():
    subprocess.call('clear', shell=True)

### ------- MAIN
debug = ask_ok("Debug? ",0)

app = raw_input("App name? [wp]")
if not app:
    app = "wp" #Default value
env = raw_input("Environment? [prod]")
if not env:
    env = "prod" #Default value
num = raw_input("Instances amount? [2]")
if not num:
    num = "2" #Default value
size = raw_input("Size of the instances? [t2.micro]")
if not size:
    size = "t2.micro" #Default value
while not size in inst_type: #WHILE created to avoid misspelling instance size
    size = raw_input("Size of the instances? [t2.micro]")
    if not size:
        size = "t2.micro" 

# FOR DEBUG
if debug:
    clear()
    print("Printing parameters")
    platform.python_version()
    print(str(app) + " " + str(env) + " " + str(num) + " " + str(size))
    print("Instances allowed")
    print(str(inst_type))
    print("terraform path")
    print(str(terraform))
    print("Ansible path")
    print(str(ansible))
    print("Starting app")

# WE PRESUME AWS IS INSTALLED AND CONFIGURED. ToDo ! 
# if not cmd_exists("aws"):
#     install_aws()
# else:
#     config_aws()

# I NEVER SEARCH TERRAFORM OR ANSIBLE ! 
while not terraform:
    if not terraform:
        print("terraform not found")
        print("Please set path")
        terraform = os.path.abspath(raw_input(terraform))

if not ansible:
    print("ansible not found")
    print("Please set path")
    ansible = os.path.abspath(raw_input(ansible))

if cmd_exists("ssh-keygen"):
    os.system("ssh-keygen -t rsa -b 4096 -f ./ansible-key -N '' >> ssh_key.log")
else:
    print("ssh not found")
    print("Please set path")
    ssh = os.path.abspath(raw_input(ssh))
    os.system(ssh + "-keygen -t rsa -b 4096 -f ./ansible-key -N '' >> ssh_key.log")

if debug:
    os.system(terraform + " apply -var app_name=" + str(app) + " -var environment=" + str(env) + " -var num_servers=" + str(num) + " -var instance_type=" + str(size) + " -var ssh_key=" + str(os.system("cat ./ansible-key.pub")))
else:
    os.system(terraform + " apply -var app_name=" + str(app) + " -var environment=" + str(env) + " -var num_servers=" + str(num) + " -var instance_type=" + str(size) + " -var ssh_key=" + str(os.system("cat ./ansible-key.pub")) + " >> terraform.log")

if debug:
    os.system(ansible + " -i ansible/inventory --key-file='./ansible-key' ansible/site.yml")
else:
    os.system(ansible + " -i ansible/inventory --key-file='./ansible-key' ansible/site.yml >> ansible.log")

os.system("echo '[wordpress]' > ansible/inventory")
os.system(terraform + " output public_ip | sed -e 's/,$//' >> ansible/inventory")
print("ELB might take a few minutes to be available. Please, hold on")
os.system(terraform + "output elb_dns_name")