output "public_ip" {
  value = "${aws_instance.aws.*.public_ip}"
}

output "elb_dns_name" {
  value = "${aws_elb.aws.dns_name}"
}
