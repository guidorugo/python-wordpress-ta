variable "nginx_port" {
  description = "The port the server will use for HTTP requests"
  default = 80
}

variable "instance_type" {
  default = "t2.micro"
}

variable "num_servers" {
  default = 2
}

variable "environment" {
  default = "dev"
}

variable "ami-instance" {
  default = "ami-10547475" # Ubuntu
}

variable "app_name" {
  default = "wp"
}

variable "ssh_key" {
  default = "aws"
}