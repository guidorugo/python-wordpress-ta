provider "aws" {
	region = "us-east-2"
}

data "aws_availability_zones" "az" {}

resource "aws_key_pair" "aws" {
  key_name   = "${var.app_name}-${var.environment}"
  public_key = "${var.ssh_key}"
}

resource "aws_security_group" "main-sg" {
  name = "${var.app_name}-${var.environment}-instance-sg"

  ingress {
    from_port = "${var.nginx_port}"
    to_port = "${var.nginx_port}"
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_security_group" "elb-sg" {
  name = "${var.app_name}-${var.environment}-elb-sg"
  
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "aws" {
  count = "${var.num_servers}"
  ami = "${var.ami-instance}"
  instance_type = "${var.instance_type}"
  vpc_security_group_ids = ["${aws_security_group.main-sg.id}"]
  key_name = "${aws_key_pair.aws.key_name}"
  tags {
    Name = "${var.app_name}-${var.environment}"
    Environment = "${var.environment}"
  }
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_elb" "aws" {
  name = "${var.app_name}-${var.environment}"
  availability_zones = ["${data.aws_availability_zones.az.names}"]
  security_groups = ["${aws_security_group.elb-sg.id}"]
  instances = ["${aws_instance.aws.*.id}"]
  health_check {
    target = "TCP:${var.nginx_port}"
    healthy_threshold = 5
    unhealthy_threshold = 5
    timeout = 6
    interval = 30
  }

  listener {
    lb_protocol = "http"
    lb_port = 80
    instance_port = "${var.nginx_port}"
    instance_protocol = "http"
  }
}
